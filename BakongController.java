package com.bakong.controller;

import java.io.FileWriter;
import java.util.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RestController;
import com.bakong.model.AccountEnquiry;
import com.bakong.model.StatusResponse;
import com.jbase.jremote.*; 
import org.json.JSONObject;
 

@RestController
@RequestMapping("/api/v1/")
public class BakongController {
	
	
	
	
		 
	
	
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public String test() {
		
		
		return "hello Bakong ! "; 
		
		
	}
	 
	
	
	
	
	
	public static String t24AccInfo(String account_no) {
		String result="";
		try {
			DefaultJConnectionFactory dcf;
            JConnection con; 
            JStatement stmt; 
           
            dcf = new DefaultJConnectionFactory();
           // dcf.setHost("192.168.107.21"); // EVM5
           // dcf.setPort(20060); // EVM5 PORT API
            dcf.setHost("192.168.102.36"); // PRO
            dcf.setPort(9095); // PORT PRO
			con = dcf.getConnection(); 
			String OFS_MSG="ENQUIRY.SELECT,,/,CPB.WINGAPI.BL.ENQ,@ID:EQ=" + account_no;
			stmt = con.createStatement();
			stmt.setFetchSize(10);
			JSubroutineParameters param = new JSubroutineParameters();
			param.add(new JDynArray(OFS_MSG));
			
			JSubroutineParameters ret = new JSubroutineParameters();
			ret = con.call("CPB.EX.OFS", param); 
			result = ret.get(0).toString(); 
			System.out.println("raw T24 ===> "+result);
			result = result.replace("\n", "\"")
					.replace("\u003c1\u003e", "\"")
					.replace(":1:1", "\"")
					.replace(":", "").replace("=", ":\"").replace(",", "\",");   
		} catch (JRemoteException e) { 
			e.printStackTrace();
		} 
		return result;
	} 
	
	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> testAcc(@RequestBody AccountEnquiry acc) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapo = new HashMap<String, Object>();
		AccountEnquiry account = new AccountEnquiry();
		StatusResponse status = new StatusResponse(); 
		try {  
				/*
				 * get account info from t24
				 */ 
				String accountStr = t24AccInfo(new String(acc.getAccount_no())); 
				JSONObject obj = new JSONObject("{"+ accountStr +"\"}");  
		        String staDesc = obj.getString("STATU.DESC");
		        //****** end
		         
		        /*
		         * make request and response log in to file.
		         */
		        String reqLog = (new Date())+"=====" +"DATA request=>" + acc.getAccount_no();
		        String resLog =  (new Date())+"=====" +"DATA response=>"+ accountStr;
		        
		        System.out.println(reqLog);
		        System.out.println(resLog);
		        
		        FileWriter fw=new FileWriter("D:\\logBakongApi.txt",true); 		
		        fw.write("-------------------------\n");
				fw.write(reqLog+"\n"); 
				fw.write(resLog+"\n"); 
				fw.write("-------------------------\n");
				fw.close();
				//****** end
		        				
				if(new String(staDesc).equals("SUCCESS")){ 
			        String inacMarker = obj.getString("INACTIV.MARKER");
			        String category = obj.getString("CATEGORY");
					if( !inacMarker.equals("Y") && !category.equals("6603") && !category.equals("6005") ) {
						String accName = obj.getString("SHORT.TITLE");
				        String ccy = obj.getString("CURRENCY"); 
				        int staCode = obj.getInt("STATUS.CODE"); 
				        
//						account.setAccount(acc.getAccount());
				        account.setAccount(acc.getAccount_no());
						account.setAccount_name(accName);
						account.setAccount_currency(ccy);
						account.setBank_code("CPBPKHPP");
						account.setAccount_type("01");
						status.setCode(0);
						
						mapo.put("result", "success");
						mapo.put("product_code", "BAKONG");
						mapo.put("account_data", account);
						 
		
						map.put("status",status);
						map.put("data",mapo);
					}else {
						status.setCode(1);
						status.setErrorCode(65);
						status.setError("The account Number is not found. Please check the entered data.");
						status.setWarning("");

						map.put("status", status);
						map.put("data", null);
					}
				}else{
					status.setCode(1);
					status.setErrorCode(65);
					status.setError("The account Number is not found. Please check the entered data.");
					status.setWarning("");

					map.put("status", status);
					map.put("data", null);
				}
		} catch (Exception e) {
			status.setCode(4);
			status.setErrorCode(10);
			status.setError("JWT token has expired. Please, log in again.");
			status.setWarning("");

			map.put("status", status);
			map.put("data", null);
			e.printStackTrace();
		} 
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}   
}
