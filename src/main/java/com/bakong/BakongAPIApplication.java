package com.bakong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
@SpringBootApplication
public class BakongAPIApplication { 
	public static void main(String[] args) {
		SpringApplication.run(BakongAPIApplication.class, args); 
	}
}