package com.bakong.controller;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RestController;
import com.bakong.model.AccountEnquiry;
import com.bakong.model.StatusResponse;
import com.jbase.jremote.*; 
import org.json.JSONObject;
 

@RestController
@RequestMapping("/api/v1/")
public class BakongController {
	
	
	public static String t24AccInfo(String account_no) {
		String result="";
		try {
			DefaultJConnectionFactory dcf;
            JConnection con; 
            JStatement stmt; 
           
            dcf = new DefaultJConnectionFactory();
           // dcf.setHost("192.168.107.21"); // EVM5
            //dcf.setPort(20060); // EVM5 PORT API
            dcf.setHost("192.168.102.35"); // PRO
            dcf.setPort(9095); // PORT PRO
			con = dcf.getConnection(); 
			//String OFS_MSG="ENQUIRY.SELECT,,/,CPB.GET.ACCOUNT.INFO,@ID:EQ=" + account_no;
			String OFS_MSG="ENQUIRY.SELECT,,/,CPB.WINGAPI.BL.ENQ,@ID:EQ=" + account_no;
			stmt = con.createStatement();
			stmt.setFetchSize(10);
			JSubroutineParameters param = new JSubroutineParameters();
			param.add(new JDynArray(OFS_MSG));
			JSubroutineParameters ret = new JSubroutineParameters();
			ret = con.call("CPB.EX.OFS", param); 
			//ret = con.call("OFS.ATM", param); 
			result = ret.get(0).toString(); 
			System.out.println("raw T24 ===> "+result);
			result = result.replace("\n", "\"")
					.replace("\u003c1\u003e", "\"")
					.replace(":1:1", "\"")
					.replace(":", "").replace("=", ":\"").replace(",", "\",");   
		} catch (JRemoteException e) { 
			e.printStackTrace();
		} 
		return result;
	} 
	
	@RequestMapping(value = "/accountBk", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> testAcc(@RequestBody AccountEnquiry acc) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapo = new HashMap<String, Object>();
		AccountEnquiry account = new AccountEnquiry();
		StatusResponse status = new StatusResponse(); 
		try {  
				/*
				 * get account info from t24
				 */ 
				String accountStr = t24AccInfo(new String(acc.getAccount_no())); 
				JSONObject obj = new JSONObject("{"+ accountStr +"\"}");  
		        String staDesc = obj.getString("STATU.DESC");
		        //****** end
		         
		        /*
		         * make request and response log in to file.
		         */
		        String reqLog = (new Date())+"=====" +"DATA request=>" + acc.getAccount_no();
		        String resLog =  (new Date())+"=====" +"DATA response=>"+ accountStr;
		        
		        System.out.println(reqLog);
		        System.out.println(resLog);
		        
		        FileWriter fw=new FileWriter("D:\\logBakongApi.txt",true); 		
		        fw.write("-------------------------\n");
				fw.write(reqLog+"\n"); 
				fw.write(resLog+"\n"); 
				fw.write("-------------------------\n");
				fw.close();
				//****** end
		        				
				if(new String(staDesc).equals("SUCCESS")){ 
			        String inacMarker = obj.getString("INACTIV.MARKER");
			        String category = obj.getString("CATEGORY");
					if( !inacMarker.equals("Y") && !category.equals("6603") && !category.equals("6005") ) {
						String accName = obj.getString("SHORT.TITLE");
				        String ccy = obj.getString("CURRENCY"); 
				        int staCode = obj.getInt("STATUS.CODE"); 
				        
						account.setAccount(acc.getAccount());
				        account.setAccount(acc.getAccount_no());
						account.setAccount_name(accName);
						account.setAccount_currency(ccy);
						account.setBank_code("CPBPKHPP");
						account.setAccount_type("01");
						status.setCode(0);
						
						mapo.put("result", "success");
						mapo.put("product_code", "BAKONG");
						mapo.put("account_data", account);
						 
		
						map.put("status",status);
						map.put("data",mapo);
				
				  }else { status.setCode(1); 
				  status.setErrorCode(65); 
				  status.setError("The account Number is not found. Please check the entered data.");
				  status.setWarning("");
				  map.put("status", status); map.put("data", null); }
				 
				}else{
					status.setCode(1);
					status.setErrorCode(65);
					status.setError("The account Number is not found. Please check the entered data.");
					status.setWarning("");

					map.put("status", status);
					map.put("data", null);
				}
		} catch (Exception e) {
			status.setCode(4);
			status.setErrorCode(10);
			status.setError("JWT token has expired. Please, log in again.");
			status.setWarning("");

			map.put("status", status);
			map.put("data", null);
			e.printStackTrace();
		} 
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> account(@RequestBody AccountEnquiry acc) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapo = new HashMap<String, Object>();
		AccountEnquiry account = new AccountEnquiry();
		StatusResponse status = new StatusResponse(); 
		
		 // JDBC driver name and database URL
		final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";  
		final String DB_URL = "jdbc:sqlserver://10.18.1.150:1433;databaseName=DB_REPORT";
		
		 //  Database credentials
		    final String USER = "CPB_INT";
		    final String PASS = "123cp!@#";
		
		   Connection conn = null;
		   Statement stmt = null;
		
		   try { 
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				//STEP 3: Open a connection
			      System.out.println("Connecting to a selected database...");
			      conn = DriverManager.getConnection(DB_URL, USER, PASS);
			      System.out.println("Connected database successfully...");
			    //STEP 4: Execute a query
			      System.out.println("Creating statement...");
			      stmt = conn.createStatement();

			      String sql = "SELECT ACCOUNTID,NAME_EN, CATEGORY_CODE,CURRENCY,BALANCE  FROM DB_REPORT.dbo.RPT_SAVING_DEPO_WITH_REPORT WHERE ACCOUNTID="+acc.getAccount_no() ;
			      System.out.println(sql);
			      
			      ResultSet rs = stmt.executeQuery(sql);
			      //STEP 5: Extract data from result set
			       
			      boolean accExist=false;
			    	  while(rs.next()){
			    		  accExist=true;
					         //Retrieve by column name  
					         String accName = rs.getString("NAME_EN");
					         String accNo= rs.getString("ACCOUNTID");
					         String ccy= rs.getString("CURRENCY"); 
					         String accCategory= rs.getString("CATEGORY_CODE");
					         String balance= rs.getString("BALANCE"); 
					         
					         System.out.println("accName:"+accName);
					         System.out.println("accNo:"+accNo);
					         System.out.println("ccy:"+ccy);
					         System.out.println("accCategory:"+accCategory);
					         System.out.println("balance:"+balance);
					         
					         
					         System.out.println("account==>"+accNo);
					         
					         if( !accCategory.equals("6603") && !accCategory.equals("6005") ) { /// not allow FD,RECURING AC in bakong deposit 
					        	 account.setAccount(acc.getAccount());
							        account.setAccount(acc.getAccount_no());
									account.setAccount_name(accName);
									account.setAccount_currency(ccy);
									account.setBank_code("CPBPKHPP");
									account.setAccount_type(accCategory);
									status.setCode(0);
									
									mapo.put("result", "success");
									mapo.put("product_code", "BAKONG");
									mapo.put("account_data", account);
									 
									map.put("status",status);
									map.put("data",mapo);
					         }else {
					        	 System.out.println("The account Number is not found. Please check the entered data.");
						     		status.setCode(1); 
						     		status.setErrorCode(65); 
						     		status.setError("The account Number is not found. Please check the entered data.");
						     		status.setWarning("");
						     		map.put("status", status); 
						     		map.put("data", null);
					         } 
					      }
			       if(accExist==false) {
			    	   System.out.println("The account Number is not found. Please check the entered data.");
			     		status.setCode(1); 
			     		status.setErrorCode(65); 
			     		status.setError("The account Number is not found. Please check the entered data.");
			     		status.setWarning("");
			     		map.put("status", status); 
			     		map.put("data", null);
			       } 
			      rs.close();
			   }catch(SQLException se){
			      //Handle errors for JDBC
			      se.printStackTrace();
			   }catch(Exception e){
				   
				   status.setCode(4);
					status.setErrorCode(10);
					status.setError("JWT token has expired. Please, log in again.");
					status.setWarning("");

					map.put("status", status);
					map.put("data", null);
				   
			      //Handle errors for Class.forName
			      e.printStackTrace();
			   }finally{
			      //finally block used to close resources
			      try{
			         if(stmt!=null)
			            conn.close();
			      }catch(SQLException se){
			      }// do nothing
			      try{
			         if(conn!=null)
			            conn.close();
			      }catch(SQLException se){
			         se.printStackTrace();
			      }//end finally try
			   }//end try 
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}
}
