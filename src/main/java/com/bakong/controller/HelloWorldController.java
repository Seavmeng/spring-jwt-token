package com.bakong.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.sql.*;

@RestController
public class HelloWorldController {
 
	   
	@RequestMapping({ "/hello" })
	public void con() {
		
		 // JDBC driver name and database URL
		final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";  
		final String DB_URL = "jdbc:sqlserver://10.18.1.150:1433;databaseName=DB_REPORT";
		
		 //  Database credentials
		    final String USER = "CPB_INT";
		    final String PASS = "123cp!@#";
		
		   Connection conn = null;
		   Statement stmt = null;
		   
		   try { 
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//STEP 3: Open a connection
		      System.out.println("Connecting to a selected database...");
		      conn = DriverManager.getConnection(DB_URL, USER, PASS);
		      System.out.println("Connected database successfully...");
		    //STEP 4: Execute a query
		      System.out.println("Creating statement...");
		      stmt = conn.createStatement();

		      String sql = "SELECT NAME_EN,NAME  FROM DB_REPORT.dbo.RPT_SAVING_DEPO_WITH_REPORT WHERE ACCOUNTID='220000342904'";
		      ResultSet rs = stmt.executeQuery(sql);
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name 
		         String CIF = rs.getString("NAME_EN");
		         String NAME = rs.getString("NAME");

		         //Display values
		         System.out.print("ID: " + CIF);
		         System.out.print(", Age: " + NAME); 
		      }
		      rs.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            conn.close();
		      }catch(SQLException se){
		      }// do nothing
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");
		 
	}

}