package com.bakong.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bakong.config.JwtTokenUtil;
import com.bakong.model.JwtRequest;
import com.bakong.model.JwtResponse;
import com.bakong.model.StatusResponse;
import com.bakong.model.UserDTO;
import com.bakong.service.JwtUserDetailsService;

import java.util.Date;
import java.util.HashMap; 
import java.util.Map;
@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

	 
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		
		
		
		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	@RequestMapping(value = "/api/v1/auth", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> userStatus(@RequestBody JwtRequest authenticationRequest) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapo = new HashMap<String, Object>();
		JwtResponse jwt = new JwtResponse();
		
		StatusResponse status = new StatusResponse(); 
		try {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			 
			
			final UserDetails userDetails = userDetailsService
					.loadUserByUsername(authenticationRequest.getUsername());

			final String token = jwtTokenUtil.generateToken(userDetails);  
			
			jwt.setToken(token); 
			
			mapo.put("code", 0);
			
			
			if (jwt.getToken() != "") { 
				status.setCode(0); 
				map.put("data",jwt); 
				map.put("status",mapo);
			}  
		} catch (Exception e) {
			status.setCode(4);
			status.setErrorCode(11);
			status.setError("Authentication error. Please try again.");
			status.setWarning("");
			
			map.put("data", null);
			map.put("status", status);
			e.printStackTrace();
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
		user.setcreateDate(new Date());
		return ResponseEntity.ok(userDetailsService.save(user));
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
			 
		} catch (BadCredentialsException e) { 
			throw new Exception("INVALID_CREDENTIALS", e);
		} 
	}
}