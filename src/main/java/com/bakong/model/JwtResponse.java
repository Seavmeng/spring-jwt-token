package com.bakong.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	protected String jwttoken; 
	 
	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}
	
	public JwtResponse() {
		this.jwttoken = null; 
	}
	
	public String getToken() {
		return this.jwttoken;
	}
	public void setToken(String token){
		this.jwttoken=token;
	} 
}