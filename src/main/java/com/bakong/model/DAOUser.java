package com.bakong.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "`tblUserBakong`")  
public class DAOUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String username;
	 
	@Column
	@JsonIgnore
	private String password;

	@Column
	@JsonIgnore
	private Date createDate;
	public Date getcreateDate() {
		return createDate;
	} 
	public void setcreateDate(Date creDt) {
		this.createDate = creDt;
	}
 
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	} 
	public void setPassword(String password) {
		this.password = password;
	} 
}