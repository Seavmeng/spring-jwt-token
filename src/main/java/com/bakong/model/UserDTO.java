package com.bakong.model;

import java.util.Date;

public class UserDTO {
	private String username;
	private String password;
	private Date createDate;
	public Date getcreateDate() {
		return createDate;
	}

	public void setcreateDate(Date createTime) {
		this.createDate = createTime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}