package com.bakong.model;

public class AccountEnquiry {
	//@JsonIgnore
	public String account_no;
	public String account;
	public String account_name;
	public String account_currency;
	public String bank_code;
	 
//	public String getAccount() {
//		return account_no;
//	}
//	public void setAccount(String account) {
//		this.account_no = account;
//	}
	
	
	
	public String getAccount_name() {
		return account_name;
	}
	/**
	 * @param account_name the account_name to set
	 */
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	/**
	 * @return the account_currency
	 */
	public String getAccount_currency() {
		return account_currency;
	}
	/**
	 * @param account_currency the account_currency to set
	 */
	public void setAccount_currency(String account_currency) {
		this.account_currency = account_currency;
	}
	/**
	 * @return the bank_code
	 */
	public String getBank_code() {
		return bank_code;
	}
	/**
	 * @param bank_code the bank_code to set
	 */
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	/**
	 * @return the account_type
	 */
	public String getAccount_type() {
		return account_type;
	}
	/**
	 * @param account_type the account_type to set
	 */
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public String account_type;

	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	} 

}
